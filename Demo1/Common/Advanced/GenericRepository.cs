using System.Collections.Generic;
using System.Linq;

using Raven.Client;

namespace Common.Advanced
{
    public class GenericRepository<T> : IGenericRepository<T>
        where T : IIdentifiable
    {
        private readonly IDocumentSession session;


        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public GenericRepository(IDocumentSession session)
        {
            this.session = session;
        }


        public T Create(T entity)
        {
            this.session.Store(entity);
            return entity;
        }


        public void Delete(string id)
        {
            T entity = this.session.Load<T>(id);
            this.session.Delete(entity);
        }


        public T Get(string id)
        {
            return this.session.Load<T>(id);
        }


        public IEnumerable<T> Get()
        {
            return this.session.Query<T>().ToList();
        }


        public T Update(string id, T entity)
        {
            this.session.Store(entity, id);
            return entity;
        }
    }
}