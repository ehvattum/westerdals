﻿using System.Collections.Generic;

namespace Common.Advanced
{
    public interface IGenericRepository<T>
        where T : IIdentifiable
    {
        T Create(T entity);
        void Delete(string name);
        IEnumerable<T> Get();
        T Get(string name);
        T Update(string id, T entity);
    }
}