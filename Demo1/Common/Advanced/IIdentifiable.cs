namespace Common.Advanced
{
    public interface IIdentifiable
    {
        string Id { get; set; }
    }
}