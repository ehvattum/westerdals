﻿namespace Common
{
    public class Todo
    {
        public bool Completed { get; set; }
        public string Task { get; set; }
        public string Id { get; set; }
    }
}