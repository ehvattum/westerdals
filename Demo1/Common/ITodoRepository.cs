﻿using System.Collections.Generic;

namespace Common
{
    //TODO make this generic =D
    public interface ITodoRepository
    {
        Todo Create(Todo todo);
        void Delete(string id);
        Todo Get(string id);
        IEnumerable<Todo> Get();
        Todo Update(string id, Todo todo);
    }
}