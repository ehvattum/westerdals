﻿using Raven.Client;
using Raven.Client.Embedded;

namespace Common
{
    public static class RavenProvider
    {
        public static IDocumentStore CreateStore()
        {
            EmbeddableDocumentStore documentStore = new EmbeddableDocumentStore
            {
                DataDirectory = "Raven"
            };

            documentStore.Initialize();
            return documentStore;
        }
    }
}