﻿using System.Collections.Generic;
using System.Linq;

using Raven.Client;

namespace Common
{
    public class TodoRepository : ITodoRepository
    {
        private readonly IDocumentSession session;


        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public TodoRepository(IDocumentSession session)
        {
            this.session = session;
        }


        public Todo Create(Todo todo)
        {
            this.session.Store(todo);
            return todo;
        }


        public void Delete(string id)
        {
            var todo = this.session.Load<Todo>(id);
            this.session.Delete(todo);
        }


        public Todo Get(string id)
        {
            return this.session.Load<Todo>(id);
        }


        public IEnumerable<Todo> Get()
        {
            return this.session.Query<Todo>().ToList();
        }


        public Todo Update(string id, Todo todo)
        {
            this.session.Store(todo, id);
            return todo;
        }
    }
}