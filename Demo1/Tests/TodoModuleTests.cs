﻿using System.Collections.Generic;
using System.Linq;

using Common;

using Nancy;
using Nancy.Testing;

using NancyFx;

using NSubstitute;

using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class TodoModuleTests
    {
        private ITodoRepository repository;

        private IEnumerable<Todo> FakeTodos
        {
            get
            {
                return new[]
                {
                    new Todo
                    {
                        Task = "my first fake task",
                        Id = "tasks/1"
                    },
                    new Todo
                    {
                        Task = "my second task",
                        Id = "tasks/2",
                        Completed = true
                    }
                };
            }
        }


        private Browser GetBrowser()
        {
            this.repository = Substitute.For<ITodoRepository>();
            this.repository.Get().ReturnsForAnyArgs(FakeTodos);
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency(this.repository);
                with.Module<TodoModule>();
            }
                );
            return new Browser(bootstrapper,
                with =>
                    with.Accept("application/json"));
        }


        [Test]
        public void Delete_ContainsTwo_CallsDeleteAnd_ReturnsOK()
        {
            //Arrange
            var browser = GetBrowser();

            //Act
            var actual = browser.Delete("todos/1");

            //Assert
            Assert.That(actual.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            this.repository.Received(1).Delete("todos/1");
        }


        [Test]
        public void Get_ContainsTwo_ReturnsTwo()
        {
            //Arrange
            var browser = GetBrowser();

            //Act
            var actual = browser.Get("todos");

            //Assert
            Assert.That(actual.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var body = actual.Body.DeserializeJson<IEnumerable<Todo>>();
            Assert.That(body.Count(), Is.EqualTo(2));
        }


        [Test]
        public void Post_Valid_CallsCreate_ReturnsCreated()
        {
            //Arrange
            var browser = GetBrowser();
            this.repository.Create(Arg.Any<Todo>()).Returns(new Todo
            {
                Id = "todos/2",
                Task = "I am a new task",
                Completed = false
            });

            //Act
            var actual = browser.Post("todos",
                with => with.JsonBody(new Todo
                {
                    Task = "I am a new task"
                }));

            //Assert
            Assert.That(actual.StatusCode, Is.EqualTo(HttpStatusCode.Created));

            //Assert response body as expected
            var body = actual.Body.DeserializeJson<Todo>();
            Assert.That(body.Id, Is.EqualTo("todos/2"));
            Assert.That(body.Task, Is.EqualTo("I am a new task"));

            //Assert that the repo was called correctly
            this.repository.Received(1).Create(Arg.Is<Todo>(t =>
                t.Id == null
                && t.Task == "I am a new task"
                && t.Completed == false
                ));
        }


        [Test]
        public void Put_CallsUpdate_ReturnsUpdated()
        {
            //Arrange
            var browser = GetBrowser();
            this.repository.Update("todos/4", Arg.Any<Todo>()).Returns(new Todo
            {
                Id = "todos/4",
                Task = "I am a completed task",
                Completed = true
            });

            //Act
            var actual = browser.Put("todos/4",
                with => with.JsonBody(new Todo
                {
                    Id = "todos/4",
                    Completed = true,
                    Task = "I am a completed task"
                }));

            //Assert statuscode
            Assert.That(actual.StatusCode, Is.EqualTo(HttpStatusCode.OK));

            //Assert response body as expected
            var body = actual.Body.DeserializeJson<Todo>();
            Assert.That(body.Id, Is.EqualTo("todos/4"));
            Assert.That(body.Task, Is.EqualTo("I am a completed task"));

            //Assert that the repo was called correctly
            this.repository.Received(1).Update("todos/4",
                Arg.Is<Todo>(t =>
                    t.Id == null
                    && t.Task == "I am a completed task"
                    && t.Completed
                    ));
        }
    }
}