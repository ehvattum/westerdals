﻿using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace App
{
    public class WebApiApplication : HttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name : "Default",
                url : "{controller}/{action}/{id}",
                defaults : new { controller = "Main", action = "Index", id = UrlParameter.Optional }
                );
        }


        protected void Application_Start()
        {
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            RegisterRoutes(RouteTable.Routes);
        }
    }

  
}