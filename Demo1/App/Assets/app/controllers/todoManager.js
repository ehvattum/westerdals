﻿angular.module('todoManager', [])
    .controller('todoManagerCtrl', [
        '$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {


            var rootUrl = '/nancyfx';
            $scope.ApiName = "Nancy";
            if ($routeParams.apiservice === 'webapi') {
                rootUrl = '/asp';
                $scope.ApiName = "Web Api";
            }


            $scope.getList = function () {
                $http.get(rootUrl + '/todos')
                    .success(function (data, status, headers, config) {
                        $scope.todoList = data;
                    });
            }

            $scope.postItem = function () {
                var item =
                {
                    task: $scope.newTaskText
                };

                if ($scope.newTaskText != '') {
                    $http.post(rootUrl + '/todos/', item)
                        .success(function (data, status, headers, config) {
                            $scope.newTaskText = '';
                            $scope.todoList.push(data);
                        });
                }
            }

            //$scope.complete = function(index) {
            //    var todo = $scope.todoList[index];
            //    todo.completed = true;
            //    $http.put(rootUrl + '/' + todo.id, todo)
            //        .success(function(data, status, headers, config) {
            //            $scope.getList();
            //        });
            //}

            //$scope.update = function(index) {
            //    $http.put(rootUrl + '/' + $scope.todoList[index].id, $scope.todoList[index])
            //        .success(function(data, status, headers, config) {
            //            $scope.getList();
            //        });
            //}

            //$scope.delete = function(index) {
            //    $http.delete(rootUrl + '/' + $scope.todoList[index].id)
            //        .success(function(data, status, headers, config) {
            //            $scope.getList();
            //        });
            //}

            //Get the current user's list when the page loads.
            $scope.getList();
        }
    ]);