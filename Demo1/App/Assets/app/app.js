﻿var app = angular.module('app', [
    'ngRoute',
    'ngCookies',
    'home',
    'todoManager'
]);

app.config([
    '$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {

        //================================================
        // Routes
        //================================================
        $routeProvider.when('/home', {
            title: 'home',
            templateUrl: 'App/App/Home',
            controller: 'homeCtrl'
        });

        $routeProvider.when('/todo/:apiservice', {
            title: 'Nancy',
            templateUrl: 'App/App/TodoManager',
            controller: 'todoManagerCtrl'
        });

        $routeProvider.when('/todo/:apiservice', {
            title: 'Web Api',
            templateUrl: 'App/App/TodoManager',
            controller: 'todoManagerCtrl'
        });

        $routeProvider.otherwise({
            redirectTo: '/home'
        });
    }
]);
app.run([
    '$location', '$rootScope', function($location, $rootScope) {
        $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
            $rootScope.title = current.$$route.title;
        });
    }
]);