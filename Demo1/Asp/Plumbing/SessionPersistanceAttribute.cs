﻿using System.Net.Http;
using System.Web.Http.Filters;

using Raven.Client;

namespace Asp.Plumbing
{
    /// <summary>
    /// This filter makes sure the RavenDB session is persisted after each request is finished.
    /// It is called by MVC after each request have left the action that recieved the request.
    /// This is part of a simple, yet powerful pattern called Unit Of Work.
    /// </summary>
    public class SessionPersistanceAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            var session = actionExecutedContext.Request.GetDependencyScope()
                .GetService(typeof(IDocumentSession)) as IDocumentSession;

            if (session != null && actionExecutedContext.Exception == null)
                session.SaveChanges();

            base.OnActionExecuted(actionExecutedContext);
        }
    }
}