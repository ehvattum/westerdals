﻿using System.Web.Http;

using Asp.Plumbing;

using Common;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

using Raven.Client;

using SimpleInjector;
using SimpleInjector.Integration.WebApi;

namespace Asp
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            ConfigureIoC();
            ConfigureSerializers(config);
            ConfigureRoutes(config);
        }


        //Inversion of Control Setup
        private static void ConfigureIoC()
        {
            var container = new Container();
            // extension from the integration package:
            container.RegisterWebApiRequest<ITodoRepository, TodoRepository>();
            container.RegisterSingle(RavenProvider.CreateStore());
            container.RegisterWebApiRequest(() => container.GetInstance<IDocumentStore>().OpenSession());
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
            container.Verify();

            GlobalConfiguration.Configuration.DependencyResolver =
                new SimpleInjectorWebApiDependencyResolver(container);
        }


        // Web API routes
        private static void ConfigureRoutes(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name : "DefaultApi",
                routeTemplate : "{controller}/{id}",
                defaults : new { id = RouteParameter.Optional }
                );
            config.Filters.Add(new SessionPersistanceAttribute());
        }


        /// <summary>
        /// We want to explicitly tell the app to use our configured JsonSerializer
        /// </summary>
        /// <param name="config"></param>
        private static void ConfigureSerializers(HttpConfiguration config)
        {
            var json = config.Formatters.JsonFormatter;
            json.SerializerSettings.Formatting = Formatting.Indented;
            json.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            json.SerializerSettings.Converters.Add(new StringEnumConverter());
        }
    }
}