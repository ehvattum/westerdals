﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Asp.Plumbing;
using Common;

namespace Asp.Controllers
{
    [SessionPersistance] //filter!
    public class TodoController : ApiController
    {
        private readonly ITodoRepository repository;


        public TodoController(ITodoRepository repository)
        {
            this.repository = repository;
        }



        [HttpGet]
        [Route("todos")]
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(repository.Get());
        }




        [HttpPost]
        [Route("todos")]
        public HttpResponseMessage Post([FromBody] Todo todo)
        {
            Todo value = repository.Create(todo);
            
            return Request.CreateResponse(HttpStatusCode.Created, value);
        }
    }
}