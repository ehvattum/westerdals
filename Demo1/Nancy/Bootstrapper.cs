﻿using System;

using Common;

using Nancy;
using Nancy.Bootstrapper;
using Nancy.TinyIoc;

using Newtonsoft.Json;

using Raven.Client;

namespace NancyFx
{
    public class Bootstrapper : DefaultNancyBootstrapper
    {
        // The bootstrapper enables you to reconfigure the composition of the framework,
        // by overriding the various methods and properties.
        // For more information https://github.com/NancyFx/Nancy/wiki/Bootstrapper
        protected override void ConfigureApplicationContainer(TinyIoCContainer container)
        {
            base.ConfigureApplicationContainer(container);
            //IOC inversion of control. We want to explicitly tell the app to use our very own "CustomJsonSerializer"
            container.Register<JsonSerializer, CustomJsonSerializer>();
            
            //We are also registering an implementation of the IDocumentStore interface, it is given to us by our RavenProvider.
            container.Register(RavenProvider.CreateStore());
        }

        /// <summary>
        /// Each request needs its own RavenDB-session.
        /// this extension point in the bootstrapper is the perfect place to create and register such things
        /// </summary>
        /// <param name="container"></param>
        /// <param name="context"></param>
        protected override void ConfigureRequestContainer(TinyIoCContainer container, NancyContext context)
        {
            base.ConfigureRequestContainer(container, context);
            //This line finds the IDocumentStore, and uses it to create a session for this request.
            //This request is then registered in the requestcontainer.
            //Anything that needs an IDocumentSession during the request is given this instance.
            container.Register(container.Resolve<IDocumentStore>().OpenSession());
            container.Register<ITodoRepository, TodoRepository>();
            
        }

        /// <summary>
        /// This is where we register our custom pipeline action "PersistSession"
        /// </summary>
        /// <param name="container"></param>
        /// <param name="pipelines"></param>
        /// <param name="context"></param>
        protected override void RequestStartup(TinyIoCContainer container, IPipelines pipelines, NancyContext context)
        {
            pipelines.AfterRequest.AddItemToEndOfPipeline(PersistSession(container));
            base.RequestStartup(container, pipelines, context);
        }

        /// <summary>
        /// This pipeline item makes sure the RavenDB session is persisted after each request is finished.
        /// It is called by the NancyEngine after each request have been answered in a module.
        /// This is part of a simple, yet powerful pattern called Unit Of Work.
        /// </summary>
        private static Action<NancyContext> PersistSession(TinyIoCContainer container)
        {
          return ctx =>
            {
                IDocumentSession documentSession = container.Resolve<IDocumentSession>();
                if (ctx.Response.StatusCode != HttpStatusCode.InternalServerError)
                    documentSession.SaveChanges();
                documentSession.Dispose();
            };
        }
    }
}