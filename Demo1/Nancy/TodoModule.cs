﻿using Common;
using Nancy;
using Nancy.ModelBinding;

namespace NancyFx
{
    public class TodoModule : NancyModule
    {
        public TodoModule(ITodoRepository repository)
            : base("/todos")
        {
            Get["/"] = p => repository.Get();

          
            Post["/"] = p =>
            {
                Todo todo = this.Bind<Todo>(t => t.Id);
                todo = repository.Create(todo);
                return Negotiate.WithModel(todo).WithStatusCode(HttpStatusCode.Created);
            };
           
        }
    }
}